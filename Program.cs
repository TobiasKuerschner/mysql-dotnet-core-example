﻿using System;
using MySQLExample.database;

namespace MySQLExample
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var table = new SAKILA_CategoryController())
                Console.WriteLine(table.ReadAll().ToString());
        }
    }
}
