using System;
using MySql.Data.MySqlClient;

namespace MySQLExample.database
{
    internal abstract class MySQLConnector : IDisposable
    {
        protected static MySqlConnection Connection;
        private static readonly string ConnectionString =
            "server=localhost;user id=root;password=toor;" + 
            "persistsecurityinfo=True;port=7071;database=sakila;" + 
            "SslMode=none;";

        public MySQLConnector()
        {
            Connection = new MySqlConnection(ConnectionString);

            try
            {
                Connection.Open();
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.TraceError(e.Message);
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) 
                //       and override a finalizer below.
                // TODO: set large fields to null.
                Connection.Close();
                Connection.Dispose();

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) 
        //       above has code to free unmanaged resources.
        // ~MySQLConnector() {
        //   // Do not change this code. Put cleanup code in 
        //   // Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in 
            // Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the 
            //       finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}