using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace MySQLExample.database
{
    internal abstract class TableController : MySQLConnector
    {
        public abstract JObject ReadAll();
        public abstract JObject Read(int ID);
        public abstract JObject Update(Dictionary<string,string> _params);
        public abstract JObject Delete(int ID);
        public abstract JObject Create(Dictionary<string,string> _params);
    }
}