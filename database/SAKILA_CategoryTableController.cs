using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;

namespace MySQLExample.database
{
    internal class SAKILA_CategoryController : TableController
    {
        public override JObject Create(Dictionary<string, string> _params)
        {
            var _jobj = new JObject();
            string _temp = null;

            _jobj["RESULT"] = "ERROR: No suitable parameter.";

            if (_params.TryGetValue("NAME", out _temp))
            {
                string _sql = "INSERT INTO sakila.category (name) VALUES " +
                    "('" + _temp + "')";
                MySqlCommand _cmd = new MySqlCommand(_sql, Connection);
                _jobj["SQLCOMMAND"] = _sql;
                _jobj["RESULT"] = _cmd.ExecuteNonQuery().ToString();
            }

            return _jobj;
        }

        public override JObject Delete(int ID)
        {
            var _jobj = new JObject();

            string _sql = "DELETE FROM sakila.category WHERE category_id = " +
                ID + ";";
            MySqlCommand _cmd = new MySqlCommand(_sql, Connection);
            _jobj["SQLCOMMAND"] = _sql;
            _jobj["RESULT"] = _cmd.ExecuteNonQuery().ToString();

            return _jobj;
        }

        public override JObject Read(int ID)
        {
            var _jobj = new JObject();

            string _sql = "SELECT * FROM sakila.category WHERE category_id = " +
                ID + ";";
            MySqlCommand _cmd = new MySqlCommand(_sql, Connection);
            _jobj["SQLCOMMAND"] = _sql;

            var _res = new JObject();
            var _head = new JArray();
            _head.Add("CATEGORY_ID");
            _head.Add("NAME");
            _head.Add("LAST_UPDATE");
            _res["HEAD"] = _head;

            var _body = new JArray();

            using (MySqlDataReader reader = _cmd.ExecuteReader())
                while (reader.Read())
                {
                    var _row = new JObject();
                    _row["CATEGORY_ID"] = $"{reader["category_id"]}";
                    _row["NAME"] = $"{reader["name"]}";
                    _row["LAST_UPDATE"] = $"{reader["last_update"]}";
                    _body.Add(_row);
                }

            _res["BODY"] = _body;
            _jobj["RESULT"] = _res;

            return _jobj;
        }

        public override JObject ReadAll()
        {
            var _jobj = new JObject();

            string _sql = "SELECT * FROM sakila.category;";
            MySqlCommand _cmd = new MySqlCommand(_sql, Connection);
            _jobj["SQLCOMMAND"] = _sql;

            var _res = new JObject();
            var _head = new JArray();
            _head.Add("CATEGORY_ID");
            _head.Add("NAME");
            _head.Add("LAST_UPDATE");
            _res["HEAD"] = _head;

            var _body = new JArray();

            using (MySqlDataReader reader = _cmd.ExecuteReader())
                while (reader.Read())
                {
                    var _row = new JObject();
                    _row["CATEGORY_ID"] = $"{reader["category_id"]}";
                    _row["NAME"] = $"{reader["name"]}";
                    _row["LAST_UPDATE"] = $"{reader["last_update"]}";
                    _body.Add(_row);
                }

            _res["BODY"] = _body;
            _jobj["RESULT"] = _res;

            return _jobj;
        }

        public override JObject Update(Dictionary<string, string> _params)
        {
            var _jobj = new JObject();

            string _tempName;
            string _tempID;

            _jobj["RESULT"] = "ERROR: No suitable parameter.";

            if (_params.TryGetValue("NAME", out _tempName))
                if (_params.TryGetValue("CATEGORY_ID", out _tempID))
                {
                    string _sql = "UPDATE sakila.category SET name='" +
                        _tempName + "' WHERE category_id = " +_tempID + ";";
                    MySqlCommand _cmd = new MySqlCommand(_sql, Connection);
                    _jobj["SQLCOMMAND"] = _sql;
                    _jobj["RESULT"] = _cmd.ExecuteNonQuery().ToString();
                }

            return _jobj;
        }
    }
}